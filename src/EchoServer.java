

//JESSE http://docs.oracle.com/javase/1.5.0/docs/guide/security/jsse/samples/index.html
//SYSTEM PROPERTIES WAY http://stilius.net/java/java_ssl.php
//https://community.oracle.com/thread/2382681?start=0&tstart=0

//SYSTEM INVOKE http://stackoverflow.com/questions/7219989/java-and-ssl-certificates

//C# WAY http://www.winsocketdotnetworkprogramming.com/managediostreamreaderwriter2i.html
//C OpenSSL tool https://www.madboa.com/geek/openssl/#cert-request

import java.io.*;
import java.security.*;
import javax.net.ssl.*;

public class EchoServer implements Runnable {

	private SSLServerSocket server;
	
	public static void main(String[] args) {
		
		System.out.println("EchoServer");
		int port = 25565;
		char[] passphrase = "passphrase".toCharArray();
		
		try {
			//SSL
			//SSLv2
			//SSLv3 - POODLE, BEAST
			//TSL - BEAST
			//TSLv1.1
			//TSLv1.2
			SSLServerSocketFactory ssf = createSSLServerSocketFactory("TLSv1.2", passphrase);
			SSLServerSocket serverSocket = (SSLServerSocket)ssf.createServerSocket(port);
			serverSocket.setEnabledProtocols(new String[] {"TLSv1.1", "TLSv1.2"});
			for(String s : serverSocket.getEnabledProtocols()) System.out.println(s);
			
			try {
				//Optionally authenticate clients(client needs certificate?)
				//serverSocket.setNeedClientAuth(true);
				new Thread(new EchoServer(serverSocket)).start();

			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch(Exception e) {
			System.out.println("ServerSocket Creation Falied");
			e.printStackTrace();
		}
	}
	
	public EchoServer(SSLServerSocket server) {
		this.server = server;
	}
	
	@Override
	public void run() {
		
		SSLSocket socket;
		try {
			System.out.println("new thread waiting for client...");
			socket = (SSLSocket)server.accept();
			System.out.println("client connected");
		} catch (IOException e) {
			System.out.println("EchoServer died: " + e.getMessage());
			e.printStackTrace();
			return;
		}

		// open new thread to accept another connection
		(new Thread(this)).start();

		//listen to socket and echo its messages
		try {
			OutputStream rawOut = socket.getOutputStream();
			PrintWriter out = new PrintWriter(
					new BufferedWriter(
					new OutputStreamWriter(rawOut)));

			BufferedReader in = new BufferedReader(
					new InputStreamReader(
						socket.getInputStream()));
			
			String message = null;
			while((message = in.readLine()) != null) {
				System.out.println(message);
				System.out.flush();
			}
			socket.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static SSLServerSocketFactory createSSLServerSocketFactory(String type, char[] passphrase) throws Exception {
		System.out.println("getting TLS Factory");
		//JESSE http://docs.oracle.com/javase/1.5.0/docs/guide/security/jsse/samples/index.html
		// set up key manager to do server authentication

		SSLContext ctx = SSLContext.getInstance(type);
		KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
		KeyStore ks = KeyStore.getInstance("JKS");
		
		ks.load(new FileInputStream("echo.jks"), passphrase);
		kmf.init(ks, passphrase);
		ctx.init(kmf.getKeyManagers(), null, null);

		return ctx.getServerSocketFactory();
		
		/* Alternative way of getting a factory
		System.out.println("getting default SSL Factory");
		//-Djavax.net.ssl.keyStore=echo.jks -Djavax.net.ssl.keyStorePassword=passphrase
		System.setProperty("javax.net.ssl.keyStore", "echo.jks");
		System.setProperty("javax.net.ssl.keyStorePassword", "passphrase");
		return SSLServerSocketFactory.getDefault();
		*/
	}
}
