

import java.io.*;
import java.security.*;
import javax.net.ssl.*;


public class EchoClient {
	
	public static void main(String[] args) {
		String host = "localhost";
		int port = 25565;
		char[] passphrase = "passphrase".toCharArray();
		
		try
		{
			SSLSocketFactory factory = createSSLSocketFactory("TLSv1.2", passphrase);			
			System.out.println("trying to connect to " + host);
			SSLSocket socket = (SSLSocket)factory.createSocket(host, port);
			
			for(String s : socket.getEnabledProtocols()) System.out.println(s);
			
			//socket.setEnabledProtocols(new String[] {"TLSv1", "TLSv1.1", "TLSv1.2"});
			
			try {
				System.out.println("handshaking...");
				socket.startHandshake();
				System.out.println("connection successful!");
				
				BufferedReader input = new BufferedReader(
						new InputStreamReader(System.in));
				
				BufferedWriter output = new BufferedWriter(
										new OutputStreamWriter(
											socket.getOutputStream()));
				
				System.out.println("Enter a message");
				
				String message = null;
				while((message = input.readLine()) != null) {
					output.write(message + '\n');
					output.flush();
				}
			} catch(Exception e) {
				e.printStackTrace();
			} finally {
				socket.close();
			}
			
		} catch(Exception e) {
			System.out.println("Falied to create socket");
			e.printStackTrace();
		}
	}
	
	public static SSLSocketFactory createSSLSocketFactory(String type, char[] passphrase) throws Exception {			
		SSLContext ctx = SSLContext.getInstance(type);
		TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
		KeyStore ts = KeyStore.getInstance("JKS");
			
		ts.load(new FileInputStream("echo.jks"), passphrase);
		tmf.init(ts);
		ctx.init(null, tmf.getTrustManagers(), null);
			
		return ctx.getSocketFactory();
	
	/* Alternative way of getting factory
		System.out.println("getting default SSL Factory");
		//alternatively run vm with:
		//-Djavax.net.ssl.trustStore=echo.jks -Djavax.net.ssl.trustStorePassword=passphrase
		System.setProperty("javax.net.ssl.trustStore", "echo.jks");
		System.setProperty("javax.net.ssl.trustStorePassword", "passphrase");
		return SSLSocketFactory.getDefault();
	*/
	}
}
